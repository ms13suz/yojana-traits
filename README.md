## Yojana common trait for multiple project.

## Require Packages

```
    composer require webpatser/laravel-uuid niklasravnsborg/laravel-pdf maatwebsite/excel guzzlehttp/guzzle
```

## Installation

```
    composer require yojana/traits
```

## Usages

```php

//SINGLE IMPORT
use Yojana\Traits\CommonTrait;

// OR MULTIPLE IMPORT, PHP 7+ code

use Yojana\Traits\{CommonTrait, FileUploadTrait,HelperTrait,ImageUploadTrait,RepoTrait,SecurityTrait}

```
