<?php

namespace Yojana\Traits\Middleware;

use Closure;
use Yojana\Traits\ConsumeExternalService;

class CheckValidTokenMiddleware
{
    use ConsumeExternalService;

    public function __construct()
    {
        $this->baseUri = config('services.auth.base_uri');
    }


    public function handle($request, Closure $next)
    {
        $headersParams = apache_request_headers();

       $response = $this->checkValidAccessToken('/sms/check-valid-token');
       
       if($response && $response['success'] === true){
            return $next($request);
       }

       return $response;
    }
}