<?php

namespace Yojana\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Illuminate\Support\Carbon;
use Webpatser\Uuid\Uuid;
// use Excel;
use Maatwebsite\Excel\Facades\Excel;
use Yojana\Traits\Exports\ExcelExport;

trait CommonTrait
{
    /* error response for form requests*/
    public function validation_response($errors)
    {
        $response = [
            'success' => false,
            'message' => $errors,
            'code' => 422
        ];
        return $response;
    }

    public function error_response($message)
    {
        $response = [
            'success' => false,
            'message' => $message,
            'code' => 500
        ];
        return $response;
    }

    public function success_response($data, $message = NULL)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data' => $data,
            'code' => 200
        ];
        return $response;
    }


    public function set_stripe_key()
    {
        info('key:' . config('general.stripe_secret_key'));
        \Stripe\Stripe::setApiKey(config('general.stripe_secret_key'));
        \Stripe\Stripe::setApiVersion(config('general.stripe_api_version'));
    }

    public function get_uuid()
    {
        return Uuid::generate()->string;
    }

    public function get_role($user)
    {
        $roles = $user->getRoleNames();
        return $roles[0];
    }

    public function now()
    {
        return Carbon::now()->toDateTimeString();
    }

    public function pagination($data, $current_page, $path = NULL, $query_string = [])
    {
        $paginate = config('general.paginate');
        $offSet = ($current_page * $paginate) - $paginate;
        $itemsForCurrentPage = array_slice($data, $offSet, $paginate);
        if ($path) {
            $response = new LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, NULL, ['path' => $path]);
            if (!empty($query_string)) {
                $response->appends($query_string);
            }
        } else {
            $response = new LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate);
        }

        return $response;
    }

    public function getMonthsList($max)
    {
        $date = Carbon::now();
        $dates = [];
        for ($i = 1; $i <= $max; $i++) {
            $dates[] = ['name' => $date->format('F Y'), 'value' => $date->format('Y-m-01')];
            $date->addMonth(1);
        }
        return $dates;
    }

    public function getCountries()
    {
        $response = [];
        $countries = config('countries');
        if (!empty($countries)) {
            foreach ($countries as $key => $country) {
                $response[$key]['name'] = ucwords(strtolower($country['name']));
                $response[$key]['code'] = $country['code'];
            }
        }
        return $response;
    }


    public function formatAddress($account)
    {
        $address = '';
        if ($account->company_address_line1) {
            $address .= $account->company_address_line1 . ' ,';
        }
        if ($account->company_city) {
            $address .= $account->company_city . ' ,';
        }
        if ($account->company_country) {
            $address .= $account->company_country;
        }
        return $address;
    }

    public function to2Decimals($value = '')
    {
        return number_format((float)$value, 2, '.', '');
    }


    public function makeDirectory($foldername)
    {
        if (!file_exists(public_path($foldername)) && !is_dir(public_path($foldername))) {
            mkdir(public_path($foldername), 0777, true);
        }
    }

    public function unlinkFileIfExists($filename)
    {
        if (file_exists(public_path($filename))) {
            unlink(public_path($filename));
        }
    }

    public function generatePdf($view, $data, $destination)
    {
        try {
            $foldername = 'storage/'.$destination['foldername'];
            $filename = 'storage/'.$destination['filename'];

            $this->makeDirectory($foldername);
            $this->unlinkFileIfExists($filename);

            PDF::loadView($view, ['data'=>$data,'type'=>'pdf'])->save(public_path($filename));

            if (file_exists(public_path($filename))) {
                return $this->success_response(asset($filename),'Pdf generated successfully');
            } else {
                return $this->error_response('Error in generating pdf.');
            }
        } catch (\Throwable $t) {
            return $this->error_response($t->getMessage());
        }
    }

    public function generateExcel($view, $data, $destination)
    {
        try {
            $foldername = $destination['foldername'];
            $filename = $destination['filename'];

            $this->makeDirectory('storage/'.$foldername);
            $this->unlinkFileIfExists($filename);

            Excel::store(new ExcelExport($view,$data), $filename, 'public');
           
            if (file_exists(public_path('storage/'.$filename))) {
                return $this->success_response(asset('storage/'.$filename),'Excel generated successfully');
            } else {
                return $this->error_response('Error in generating excel.');
            }

        } catch (\Throwable $t) {
            return $this->error_response($t->getMessage());
        }
    }

    public function isValidHttpStatusCode($code)
    {
        /**
        * Content from http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        **/
        $valid_codes =  array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            102 => 'Processing', // WebDAV; RFC 2518
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information', // since HTTP/1.1
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            207 => 'Multi-Status', // WebDAV; RFC 4918
            208 => 'Already Reported', // WebDAV; RFC 5842
            226 => 'IM Used', // RFC 3229
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other', // since HTTP/1.1
            304 => 'Not Modified',
            305 => 'Use Proxy', // since HTTP/1.1
            306 => 'Switch Proxy',
            307 => 'Temporary Redirect', // since HTTP/1.1
            308 => 'Permanent Redirect', // approved as experimental RFC
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            418 => 'I\'m a teapot', // RFC 2324
            419 => 'Authentication Timeout', // not in RFC 2616
            420 => 'Enhance Your Calm', // Twitter
            420 => 'Method Failure', // Spring Framework
            422 => 'Unprocessable Entity', // WebDAV; RFC 4918
            423 => 'Locked', // WebDAV; RFC 4918
            424 => 'Failed Dependency', // WebDAV; RFC 4918
            424 => 'Method Failure', // WebDAV)
            425 => 'Unordered Collection', // Internet draft
            426 => 'Upgrade Required', // RFC 2817
            428 => 'Precondition Required', // RFC 6585
            429 => 'Too Many Requests', // RFC 6585
            431 => 'Request Header Fields Too Large', // RFC 6585
            444 => 'No Response', // Nginx
            449 => 'Retry With', // Microsoft
            450 => 'Blocked by Windows Parental Controls', // Microsoft
            451 => 'Redirect', // Microsoft
            451 => 'Unavailable For Legal Reasons', // Internet draft
            494 => 'Request Header Too Large', // Nginx
            495 => 'Cert Error', // Nginx
            496 => 'No Cert', // Nginx
            497 => 'HTTP to HTTPS', // Nginx
            499 => 'Client Closed Request', // Nginx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            506 => 'Variant Also Negotiates', // RFC 2295
            507 => 'Insufficient Storage', // WebDAV; RFC 4918
            508 => 'Loop Detected', // WebDAV; RFC 5842
            509 => 'Bandwidth Limit Exceeded', // Apache bw/limited extension
            510 => 'Not Extended', // RFC 2774
            511 => 'Network Authentication Required', // RFC 6585
            598 => 'Network read timeout error', // Unknown
            599 => 'Network connect timeout error', // Unknown
        );

        return keyExists($code, $valid_codes);
    }

    /**
   * Get current ip address.
   *
    * @return object
    */
    public function get_real_ipaddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip=$_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    /**
     * Get current mac address.
     *
     * @return object
     */
    public function get_Mac_Address(){
        ob_start();
        system('ipconfig-a');
        $mycomsys=ob_get_contents();
        ob_clean();
        $find_mac = "Physical";
        $pmac = strpos($mycomsys, $find_mac);
        $macaddress=substr($mycomsys,($pmac+37),18);
        return $macaddress;
    }
}


