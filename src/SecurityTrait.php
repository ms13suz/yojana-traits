<?php

namespace Yojana\Traits;

trait SecurityTrait
{
    public function checkUser($user, $user_type = NULL)
    {
        if (empty($user)) {
            return $this->error_response('Such user does not exist.');
        }
        // dd($user);
        if (!is_null($user_type)) {
            if (!$this->checkUserType($user, $user_type)) {
                return $this->error_response('User with email ' . $user->email . ' and ' . $user_type . ' role does not exist.');
            }
        }
        return $this->success_response(NULL, NULL);
    }

    public function checkAdminType($user)
    {
        return ($user->user_type == 'admin') ? true : false;
    }


    public function checkUserType($user, $type)
    {
        // dd($user->user_type);
        return ($user->user_type == $type) ? true : false;
    }
}


