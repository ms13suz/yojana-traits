<?php

namespace Yojana\Traits\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExcelExport implements FromView
{
    private $data;
    private $view;

    public function __construct($view,$data)
    {
        $this->view = $view;
        $this->data = $data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view($this->view, ['data'=>$this->data]);
    }
}
