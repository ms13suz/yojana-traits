<?php

namespace Yojana\Traits;

trait RepoTrait
{
    public function error($message=null, $code=null)
    {
        if(is_null($code)) $code=500;
        if(is_null($message)) $message='ERROR OCCURRED';

        throw new \Exception($message, $code);
    }

    public function error_exists($message=null, $code=null)
    {
        if(is_null($code)) $code=406;
        if(is_null($message)) $message='Data already exsits.';

        throw new \Exception($message, $code);
    }

    public function error_404($message=null, $code=null)
    {
        if(is_null($code)) $code=400;
        if(is_null($message)) $message='ERROR OCCURRED';

        throw new \Exception($message, $code);
    }
}