<?php

namespace Yojana\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Illuminate\Support\Carbon;
use Webpatser\Uuid\Uuid;

trait HelperTrait
{
    public function getFileSizeReadableString($sizeInBytes)
    {
        if ($sizeInBytes >= 1073741824)
        {
            $bytes = number_format($sizeInBytes / 1073741824, 2) . ' GB';
        }
        elseif ($sizeInBytes >= 1048576)
        {
            $bytes = number_format($sizeInBytes / 1048576, 1) . ' MB';
        }
        elseif ($sizeInBytes >= 1024)
        {
            $bytes = number_format($sizeInBytes / 1024) . ' KB';
        }
        elseif ($sizeInBytes > 1)
        {
            $bytes = $sizeInBytes . ' bytes';
        }
        elseif ($sizeInBytes == 1)
        {
            $bytes = $sizeInBytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}


