<?php

namespace Yojana\Traits;

use Illuminate\Support\Facades\Http;

trait ConsumeExternalService
{
    /**
     * Send request to any service
     * @method POST
     * @param $requestUrl
     * @param array $formParams
     * @param array $headers
     * @return string
     */
    public function performRequest($method, $requestUrl, $formParams = [], $headers = [])
    {
        $headersParams = apache_request_headers();

        if(keyExists($headersParams,'Authorization'))
        {
            $headers['Authorization'] = $headersParams['Authorization'];
        }

        $url = $this->baseUri.''.$requestUrl;

        $response = 'INVALID';

        if($method === 'POST')
            $response = Http::withHeaders($headers)->post($url, $formParams);
        else if($method === 'GET')
            $response = Http::withHeaders($headers)->get($url, $formParams);
        else if($method === 'PUT')
            $response = Http::withHeaders($headers)->put($url, $formParams);
        else if($method === "DELETE")
            $response = Http::withHeaders($headers)->delete($url, $formParams);

        if($response === 'INVALID') abort('The HTTP method is not valid',500);

        return $this->sendResponse($response);
    }

    public function sendResponse($result)
    {
        if($result->successful()) return $result->json();

        if($result->failed()) $result->throw()->json();
    }

    /**
     * CHECK VALID TOKEN 
     * @method POST
     * @param $requestUrl
     * @param array $headers
     * @return string
     */
    public function checkValidAccessToken($requestUrl, $formParams = [], $headers = [])
    {
        $headersParams = apache_request_headers();

        if(keyExists($headersParams,'Authorization'))
        {
            $headers['Authorization'] = $headersParams['Authorization'];
        }else{
            abort(500, "No authorization header found");
        }

        $url = $this->baseUri.''.$requestUrl;

        $response = Http::withHeaders($headers)->post($url, $formParams);

        return $this->sendResponse($response);
    }

    // public function performRequest($method, $requestUrl, $formParams = [], $headers = [])
    // {
    //     $headersParams = apache_request_headers();

    //     $client = new Client([
    //         'base_uri'  =>  $this->baseUri,
    //     ]);

    //     if(keyExists($headersParams,'Authorization'))
    //     {
    //         $headers['Authorization'] = $headersParams['Authorization'];
    //     }

    //     $response = $client->request($method, $requestUrl, [
    //         'form_params' => $formParams,
    //         'headers'     => $headers,
    //     ]);
    //     return $response->getBody()->getContents();
    // }

}